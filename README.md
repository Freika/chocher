# Chocher

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `chocher` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:chocher, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/chocher](https://hexdocs.pm/chocher).

## Release

```
export MIX_ENV=prod
mix deps.get
mix release
```

## Usage

```
$ _build/prod/rel/bakeware/chocher --file urls.txt
ok
```

urls.txt must be a txt file containing links to google images. If response body contains test indicating image is missing, the url will be added to list and in the end of process resulting list will be written into file 404s.txt.
