defmodule Chocher do
  def main(filepath) do
    {:ok, contents} = File.read(filepath)

    urls = contents |> String.split("\n", trim: true) |> Enum.map(fn x -> x <> "&hl=en" end  )

    page_not_found_line = "The page you were on is trying to send you to an invalid URL"

    notfounds = Enum.map(urls, fn url ->
      {:ok, response} = HTTPoison.get(url)

      case String.contains?(response.body, [page_not_found_line]) do
        true -> "#{url}\n"
        _ -> nil
      end
    end) |> Enum.filter(fn x -> x != nil end)

    File.write("404s.txt", notfounds)
  end
end
